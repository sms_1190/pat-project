package project.pattern;
/**
 * @File Name : TreeNode.java
 * @Description : Class for creating nodes of pattern
 * @author Mohit
 */
import java.util.LinkedList;
import java.util.List;

public class TreeNode {
	//variable to store String of node
	private String data;
	//variable to store children
	private List<TreeNode> children;

	public TreeNode(String data) {
		this.data = data;
		this.children=new LinkedList<TreeNode>();
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public List<TreeNode> getChildren() {
		return children;
	}

	public void setChildren(List<TreeNode> children) {
		this.children = children;
	}
	
	
}
