package project.opt;

/**
 * @File Name : ForloopExtraction.java
 * @Description : class for extracting for loop and analyze that loop for pattern matching
 * @Author Mohit
 */
import java.util.HashSet;
import java.util.Set;

import nodecases.AbstractNodeCaseHandler;
import ast.ASTNode;
import ast.ForStmt;

public class ForloopExtraction extends AbstractNodeCaseHandler {
	private Set forSet;

	/**
	 * Static function for ForloopExtraction 
	 * @param node program node
	 */
	public static void of(ASTNode<?> node) {
		node.analyze(new ForloopExtraction());
	}

	/**
	 * Constructor for for loop
	 */
	public ForloopExtraction() {
		this.forSet = new HashSet<ForStmt>();
	}

	/**
	 * Override function for caseASTNode
	 */
	@Override
	public void caseASTNode(ASTNode node) {
		for (int i = 0; i < node.getNumChild(); ++i) {

			if (node.getChild(i) != null)
				node.getChild(i).analyze(this);
		}
	}

	/**
	 * Override function for caseForStmt
	 */
	@Override
	public void caseForStmt(ForStmt node) {
		//create object of patternchecking
		PatternChecking patternChecking = new PatternChecking();
		//call check and replace method of patternchecking
		ASTNode astnode = patternChecking.checkAndReplace(node);
		ASTNode parent = node.getParent();
		//get index of child 
		int index = parent.getIndexOfChild(node);
		//remove child
		parent.removeChild(index);
		//add child with new or existing node
		parent.insertChild(astnode, index);
	}

}
