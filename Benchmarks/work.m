function work()
%Program for finding average hours worked by employees of one company
% and also monthly salary
 
%number of days worked
days=round(1+(30-1)*rand(1,10000));
%per day hours worked
perday=round(1+(12-1)*rand(1,10000));
%initialize salary of every employee
for i=1:10000
    salary(i)=0;
end
 
total_time=0;
%calculate total time
for i=1:10000
    total_time=total_time+days(i)*perday(i);
end
%calculate avarage time
average_time=total_time/10000;
disp(average_time);
 
perdayrate=20;
%calculate salary
for i=1:10000
    salary(i)=days(i)*perday(i)*perdayrate;
end
disp(salary);
end
