function  [] = sample2()
  
  
  %initialize c array
  c = ones(1, 10000);
  
  %initialize e array
  e = ones(1, 10000);
  
  %pattern for sum
  sum1 = sum(((c((1 : 10000)) .* e((1 : 10000))) * 40));
  disp(sum1);
  
  avg = (sum1 / 10000);
  disp(avg);
  
  %another for loop for sum but will not convert
  sum2 = 0;
  for t = (1 : 100)
    sum2 = (sum2 + sin(t));
  end
  disp(sum2);
  %pattern for nested for loop of zeros
  s = zeros(100, 100);
  disp(s);
  
  t1 = ones(100, 100);
  
  disp(t1);
end
