function  [] = work()
  %Program for finding average hours worked by employees of one company
  % and also monthly salary
  
  %number of days worked
  days = round((1 + ((30 - 1) * rand(1, 10000))));
  %per day hours worked
  perday = round((1 + ((12 - 1) * rand(1, 10000))));
  %inialize salary of every employee
  salary = zeros(1, 10000);
  
  %calculate total time
  total_time = dot(days((1 : 10000)), perday((1 : 10000)));
  %calculate avarage time
  average_time = (total_time / 10000);
  disp(average_time);
  
  %calculate salary
  for i = (1 : 10000)
    salary(i) = ((days(i) * perday(i)) * 20);
  end
  disp(salary);
end
