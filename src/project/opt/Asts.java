package project.opt;
/**
 * @File Name : Asts.java
 * @Description : helper class for replacing nodes
 * @Author : Mohit Shah
 */
import ast.ASTNode;
import ast.AssignStmt;
import ast.Expr;
import ast.IntLiteralExpr;
import ast.List;
import ast.Name;
import ast.NameExpr;
import ast.ParameterizedExpr;
import ast.RangeExpr;
import ast.Stmt;

public class Asts {

	/**
	 * Function for creating function call expression
	 * @param name function name
	 * @param arg parameters of function call
	 * @return Expr
	 */
	public static Expr functionCall(String name, Expr arg) {
		List<Expr> arguments = new List<Expr>();
		arguments.add(arg);
		return new ParameterizedExpr(new NameExpr(new Name(name)), arguments);
	}
	/**
	 * Function for creating function call expression
	 * @param name function name
	 * @param arg parameters of function call
	 * @return Expr
	 */
	private static Expr functionCall(String name, List<Expr> arg) {

		return new ParameterizedExpr(new NameExpr(new Name(name)), arg);
	}

	/**
	 * Function for replacing for loop with zeros or ones library routines
	 * 
	 * @param i paramter for zeros or ones
	 * @param r rangeexpression of for loop
	 * @param varname variable name for initializing
	 * @return Statement with function call
	 */
	public static Stmt createzerosorones(int i, RangeExpr r, NameExpr varname) {
		List<Expr> arguments = new List<Expr>();

		arguments.add((IntLiteralExpr) r.getChild(0));
		arguments.add((IntLiteralExpr) r.getChild(2));
		if (i == 0) {
			return new AssignStmt(varname, functionCall("zeros", arguments));
		} else if (i == 1) {
			return new AssignStmt(varname, functionCall("ones", arguments));
		}
		return null;
	}

	/**
	 * Functions for replacing for loop with sum library routine
	 * 
	 * @param lvarname assignment variable
	 * @param varName used variable in sum function call
	 * @param r rangeexpression for for loop
	 * @return ASTNode assignment statement
	 */
	public static ASTNode createSumStmt(NameExpr lvarname, String varName,
			RangeExpr r) {
		List<Expr> arguments = new List<Expr>();
		arguments.add(r);
		return new AssignStmt(lvarname, functionCall("sum",
				functionCall(varName, arguments)));
	}

	/**
	 * 
	 * @param lvarname assignment variable
	 * @param astnode1 Whole expression for sum
	 * @return ASTNode assignment Statement
	 */
	public static ASTNode createSumStmt(NameExpr lvarname, Expr astnode1) {
		return new AssignStmt(lvarname, functionCall("sum", astnode1));
	}
	
	/**
	 * 
	 * @param lvarname  assignment variable
	 * @param f first variable
	 * @param s second variable
	 * @param r rangeexpression
	 * @return ASTNode assignment Statement
	 */
	public static ASTNode createDotStmt(NameExpr lvarname,
			NameExpr f,NameExpr s,RangeExpr r) {
		List<Expr> arguments = new List<Expr>();
		Expr first=functionCall(f.getVarName(), r);
		Expr second=functionCall(s.getVarName(),r);
		arguments.add(first);
		arguments.add(second);
		return new AssignStmt(lvarname, functionCall("dot",
				arguments));
	}

	/**
	 * Functions for replacing for loop with prod library routine
	 * 
	 * @param lvarname assignment variable
	 * @param varName used variable in prod function call
	 * @param r rangeexpression for for loop
	 * @return ASTNode assignment statement
	 */
	public static ASTNode createProdStmt(NameExpr lvarname, String varName,
			RangeExpr r) {
		List<Expr> arguments = new List<Expr>();
		arguments.add(r);
		return new AssignStmt(lvarname, functionCall("prod",
				functionCall(varName, arguments)));
	}

	/**
	 * 
	 * @param lvarname assignment variable
	 * @param astnode1 Whole expression for prod
	 * @return ASTNode assignment Statement
	 */
	public static ASTNode createProdStmt(NameExpr lvarname, Expr astnode1) {
		return new AssignStmt(lvarname, functionCall("prod", astnode1));
	}

	/**
	 * Function for replacing nested for loop with zeros or ones library routine
	 * 
	 * @param i variable for checking o or one
	 * @param varname assignment variable
	 * @param varindex first index
	 * @param varindex2 second index 
	 * @param rangeindex first index of for loop
	 * @param rangeindex2 second index of nested for loop
	 * @param range1 upper range of first for loop
	 * @param range2 upper range of second for loop
	 * @return ASTNode Assignment Statement
	 */
	public static ASTNode nestedzerosorones(int i, NameExpr varname,
			String varindex, String varindex2, String rangeindex,
			String rangeindex2, IntLiteralExpr range1, IntLiteralExpr range2) {
		List<Expr> arguments = new List<Expr>();
		if (varindex.equals(rangeindex)) {
			arguments.add(range1);
		} else if (varindex.equals(rangeindex2)) {
			arguments.add(range2);
		}
		if (varindex2.equals(rangeindex)) {
			arguments.add(range1);
		} else if (varindex2.equals(rangeindex2)) {
			arguments.add(range2);
		}
		if (i == 0) {
			return new AssignStmt(varname, functionCall("zeros", arguments));
		} else if (i == 1) {
			return new AssignStmt(varname, functionCall("ones", arguments));
		}
		return null;
	}

}
