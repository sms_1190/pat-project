function calcelectricityconsumption()
%power of devices
w=round(1+(100-1)*rand(1,10000))*10;
%time in hour
time=round(1+(24-1)*rand(1,10000));
% charge in $ per kWH
charge=0.109;
 
%initialize total_electricity charge
total_electricity_charge=0;
 
%calculate total electricity charges
for i=1:10000
    total_electricity_charge=total_electricity_charge+((w(i).*time(i)./1000)).*charge;
end
%display total electricity consumption
disp(total_electricity_charge);
end
