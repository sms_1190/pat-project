function  [] = sample()
  
  a = zeros(1, 10);
  disp(a);
  
  c = ones(1, 10);
  
  d = ones(1, 10);
  dd = 0;
  dd = dot(c((1 : 10)), d((1 : 10)));
  disp(dd);
  
  prod1 = 1;
  prod1 = prod(((c((1 : 5)) .* d((1 : 5))) * 100));
  disp(prod1);
  
  sum1 = 0;
  sum1 = sum(((c((1 : 5)) .* d((1 : 5))) * 40));
  disp(sum1);
  
  sum2 = 0;
  for t = (1 : 10)
    sum2 = (sum2 + sin(t));
  end
  
  avg = (sum2 / 10);
  disp(avg);
  
  s = zeros(100, 50);
  disp(s);
  s = zeros(50, 100);
  
  disp(s);
end
