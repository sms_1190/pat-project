package project.opt;

/**
 * File Name : ForwardSubstitution.java
 * @description : Class for Forward Substitution
 * If the use of variable found, replace with its definition,
 * @author Mohit 
 */
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import natlab.toolkits.analysis.HashMapFlowMap;
import natlab.toolkits.analysis.Merger;
import natlab.toolkits.analysis.Mergers;
import nodecases.AbstractNodeCaseHandler;
import analysis.AbstractSimpleStructuralForwardAnalysis;
import ast.ASTNode;
import ast.AssignStmt;
import ast.EmptyStmt;
import ast.NameExpr;
import ast.Stmt;


public class ForwardSubstitution
		extends
		AbstractSimpleStructuralForwardAnalysis<HashMapFlowMap<String, Set<AssignStmt>>> {
	// Factory method, instantiates and runs the analysis
	public static ForwardSubstitution of(ASTNode<?> tree) {
		
		ForwardSubstitution analysis = new ForwardSubstitution(tree);
		analysis.analyze();
		return analysis;
	}

	public void nextStage() {
		getTree().analyze(this.new Intst());
		// after replacing usage with defs, do deadcode elimination
		DeadCodeAnalysis.of(getTree()).nextStage(0);
	}

	private ForwardSubstitution(ASTNode tree) {
		super(tree);
	}

	/**
	 * Override function for initializing new initial flow
	 */
	@Override
	public HashMapFlowMap<String, Set<AssignStmt>> newInitialFlow() {
		return new HashMapFlowMap<String, Set<AssignStmt>>();
	}

	/**
	 * Override function for caseStmt
	 */
	@Override
	public void caseStmt(Stmt node) {
		inFlowSets.put(node, currentInSet.copy());
		currentInSet.copy(currentOutSet);
		outFlowSets.put(node, currentOutSet.copy());
	}

	/**
	 * Override function for caseAssignStmt
	 */
	@Override
	public void caseAssignStmt(AssignStmt node) {
		inFlowSets.put(node, currentInSet.copy());

		// kill. We kill all previous definitions of variables defined by this
		// statement.
		// (We don't need the actual defs, just the variables, since we can just
		// remove by key
		// in the map). Gathering up the variables can be fairly complicated if
		// we're just
		// working with the AST without simplifications; you can have e.g.
		// * multiple assignments: [x, y] = ...
		// * complicated lvalues: a(i).b = ...
		// The getLValues() method takes care of all the cases.
		Set<String> kill = node.getLValues();

		// gen just maps every lvalue to a set containing this statement.
		HashMapFlowMap<String, Set<AssignStmt>> gen = newInitialFlow();
		for (String s : node.getLValues()) {
			Set<AssignStmt> defs = new HashSet<AssignStmt>();
			defs.add(node);
			gen.put(s, defs);
		}

		// compute (in - kill) + gen
		currentOutSet = newInitialFlow();
		currentInSet.copy(currentOutSet);
		currentOutSet.removeKeys(kill);
		currentOutSet.union(gen);

		outFlowSets.put(node, currentOutSet.copy());
	}

/**
 * Override function for copy
 */
	@Override
	public void copy(HashMapFlowMap<String, Set<AssignStmt>> src,
			HashMapFlowMap<String, Set<AssignStmt>> dest) {
		src.copy(dest);
	}


	private static final Merger<Set<AssignStmt>> UNION = Mergers.union();
	/**
	 * Override function for merge
	 */
	@Override
	public void merge(HashMapFlowMap<String, Set<AssignStmt>> in1,
			HashMapFlowMap<String, Set<AssignStmt>> in2,
			HashMapFlowMap<String, Set<AssignStmt>> out) {

		in1.union(UNION, in2, out);
	}

	/**
	 * class for changing usage with defs after doing rechaing definition
	 * analysis
	 * 
	 * @author Mohit
	 * 
	 */
	class Intst extends AbstractNodeCaseHandler {
		/**
		 * Override function for caseAssignStmt
		 */
		@Override
		public void caseAssignStmt(AssignStmt node) {

			HashMapFlowMap<String, Set<AssignStmt>> inset = inFlowSets
					.get(node);
			// create object of copyAssignmentChanger class
			CopyAssignmentChanger copyAssignmentChanger = new CopyAssignmentChanger();
			String t1 = node.getChild(1).toString();
			System.out.println(t1);
			// check for not range expression
			if (!t1.substring(t1.indexOf('.') + 1, t1.indexOf('@')).equals(
					"RangeExpr")) {
				// call change function to change usage with defs
				copyAssignmentChanger.changer(node.getChild(1), inset);
			}
		}

		/**
		 * Override function for caseASTNode
		 */
		@Override
		public void caseASTNode(ASTNode node) {

			for (int i = 0; i < node.getNumChild(); ++i) {

				if (node.getChild(i) != null)
					node.getChild(i).analyze(this);
			}
		}
	}
}
