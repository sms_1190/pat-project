package project.opt;

/**
 * @File Name : ConstantPropagationAnalysis.java
 * @Description : Class for doing constant propagation  analysis and replace usage 
 * 				with constants values
 * @author Mohit
 */
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import natlab.toolkits.analysis.HashMapFlowMap;
import natlab.toolkits.analysis.Merger;
import natlab.toolkits.analysis.Mergers;
import natlab.utils.NodeFinder;
import nodecases.AbstractNodeCaseHandler;
import analysis.AbstractSimpleStructuralForwardAnalysis;
import ast.ASTNode;
import ast.AssignStmt;
import ast.BinaryExpr;
import ast.Expr;
import ast.LiteralExpr;
import ast.NameExpr;
import ast.Stmt;

//forward analysis
public class ConstantPropogationAnalysis
		extends
		AbstractSimpleStructuralForwardAnalysis<HashMapFlowMap<String, Set<LiteralExpr>>> {

	/**
	 * Functio for analysing Constant Propagation
	 * @param tree program node
	 * @return analysis
	 */
	public static ConstantPropogationAnalysis of(ASTNode<?> tree) {
		ConstantPropogationAnalysis analysis = new ConstantPropogationAnalysis(
				tree);
		analysis.analyze();
		return analysis;
	}

	/**
	 * Function for converting code after doing constant propagation
	 */
	public void nextStage() {
		getTree().analyze(this.new Intst());
		// After converting the code,analyze for copy expressions
		ForwardSubstitution.of(getTree()).nextStage();
	}

	// Constructor for Constatnt Propagation analysis
	private ConstantPropogationAnalysis(ASTNode tree) {
		super(tree);
		// inialize currentInSet and cuurentOutSet
		currentInSet = newInitialFlow();
		currentOutSet = newInitialFlow();
	}

	// The initial flow is an empty map.
	@Override
	public HashMapFlowMap<String, Set<LiteralExpr>> newInitialFlow() {
		return new HashMapFlowMap<String, Set<LiteralExpr>>();
	}
	/**
	 * Override function for caseStmt
	 */
	@Override
	public void caseStmt(Stmt node) {
		inFlowSets.put(node, currentInSet.copy());
		currentInSet.copy(currentOutSet);
		outFlowSets.put(node, currentOutSet.copy());
	}

	/**
	 * Override function for caseAssignStmt
	 */
	@Override
	public void caseAssignStmt(AssignStmt node) {

		inFlowSets.put(node, currentInSet.copy());
		// find the kill set which is defs of variable
		Set<String> kill = new HashSet<String>();
		HashMapFlowMap<String, Set<LiteralExpr>> gen = newInitialFlow();

		Iterator<NameExpr> itr3 = node.getLHS().getAllNameExpressions()
				.iterator();
		while (itr3.hasNext()) {
			String temp = itr3.next().getVarName();
			kill.add(temp);
		}

		ASTNode expr = node.getChild(1);

		// if the RHS of assignment statement binary expression, evaluate binary
		// expression
		// for getting new defs of variable on the LHS
		if (expr instanceof BinaryExpr) {
			// create object of BinaryExprEvaluate class
			BinaryExprEvaluate bee = new BinaryExprEvaluate();
			// evaluate Expression and get new defs of variable
			Expr ev = bee.evaluate((Expr) expr, currentInSet);

			// if the evaluated expression is literal,then add it into gen set
			if (ev instanceof LiteralExpr) {
				// y=10; like expression

				Iterator<String> itr2 = node.getLValues().iterator();
				Set<LiteralExpr> defs = new HashSet<LiteralExpr>();
				defs.add((LiteralExpr) ev);
				String temp = itr2.next();

				gen.put(temp, defs);

			}

		} else {
			// if not binary expression, it can be Literal or NameExpr

			Iterable<LiteralExpr> t = NodeFinder.find(LiteralExpr.class, node);
			Iterator<LiteralExpr> itr = t.iterator();
			// x=10
			// if Literal,add this def into gen set
			if (itr.hasNext()) {
				LiteralExpr lval = itr.next();
				Iterator<String> itr2 = node.getLValues().iterator();
				Set<LiteralExpr> defs = new HashSet<LiteralExpr>();
				defs.add(lval);
				gen.put(itr2.next(), defs);

			} else {
				// y=x
				// if it is name expression ,check for the presence of that
				// variable
				if (expr instanceof NameExpr) {
					Iterator<NameExpr> itr2 = node.getRHS()
							.getAllNameExpressions().iterator();

					String temp = itr2.next().getVarName();
					// if present in the currentInSet, add new def into gen set
					if (currentInSet.containsKey(temp)) {
						Set<LiteralExpr> getvalues = currentInSet.get(temp);
						Iterator<LiteralExpr> itr4 = getvalues.iterator();
						if (itr4.hasNext()) {
							LiteralExpr val = itr4.next();
							Iterator<String> itr5 = node.getLValues()
									.iterator();
							Set<LiteralExpr> defs = new HashSet<LiteralExpr>();
							defs.add(val);
							gen.put(itr5.next(), defs);
						}
					}
				}
			}
		}

		currentInSet.copy(currentOutSet);
		currentOutSet.removeKeys(kill);
		Iterator<String> genval = gen.keySet().iterator();
		while (genval.hasNext()) {
			String t1 = genval.next();

			currentOutSet.put(t1, gen.get(t1));
		}
		outFlowSets.put(node, currentOutSet.copy());

	}

	/**
	 * Override function for copy
	 */
	@Override
	public void copy(HashMapFlowMap<String, Set<LiteralExpr>> src,
			HashMapFlowMap<String, Set<LiteralExpr>> dest) {
		src.copy(dest);
	}

	
	private static final Merger<Set<LiteralExpr>> UNION = Mergers.union();

	/**
	 * Override function for merge
	 */
	@Override
	public void merge(HashMapFlowMap<String, Set<LiteralExpr>> in1,
			HashMapFlowMap<String, Set<LiteralExpr>> in2,
			HashMapFlowMap<String, Set<LiteralExpr>> out) {
		in1.union(UNION, in2, out);
		// if after merging variable has more than two defs, set as T (null)
		Iterator<String> rr = out.keySet().iterator();
		while (rr.hasNext()) {
			String r1 = rr.next();
			Iterator it = out.get(r1).iterator();
			int count = 0;
			while (it.hasNext()) {
				count++;
				it.next();
			}
			if (count > 1) {
				Set<LiteralExpr> s1 = new HashSet<LiteralExpr>();
				s1.add(null);
				out.put(r1, s1);
			}
		}
	}

	/**
	 * class for replacing constants
	 * 
	 * @author Mohit
	 * 
	 */
	class Intst extends AbstractNodeCaseHandler {
		public void instrument(ASTNode<?> node) {
			node.analyze(new Intst());
		}

		/**
		 * Override function for caseAssignStmt
		 */
		@Override
		public void caseAssignStmt(AssignStmt node) {

			HashMapFlowMap<String, Set<LiteralExpr>> inset = inFlowSets
					.get(node);

			ASTNode expr = node.getChild(1);
			// set new binary expressin after replacing some variable in that
			// expression
			if (expr instanceof BinaryExpr) {
				boolean flag = false;
				BinaryExpr bexpr = (BinaryExpr) expr;
				BinaryExprEvaluate bee = new BinaryExprEvaluate();
				Expr ev = bee.evaluate((Expr) expr, inset);
				node.setChild(ev, 1);
			} else {

				Iterable<LiteralExpr> t = NodeFinder.find(LiteralExpr.class,
						node);
				Iterator<LiteralExpr> itr = t.iterator();
				// x=10
				if (!(itr.hasNext())) {
					// y=x
					if (expr instanceof NameExpr) {
						Iterator<NameExpr> itr2 = node.getRHS()
								.getAllNameExpressions().iterator();

						String temp = itr2.next().getVarName();
						if (inset.containsKey(temp)) {
							Set<LiteralExpr> getvalues = inset.get(temp);
							Iterator<LiteralExpr> itr4 = getvalues.iterator();
							if (itr4.hasNext()) {
								LiteralExpr val = itr4.next();
								if (val != null) {
									node.setRHS(val);
								}
							}
						}
					}
				}
			}
		}

		/**
		 * Override function for caseASTNode
		 */
		@Override
		public void caseASTNode(ASTNode node) {
			for (int i = 0; i < node.getNumChild(); ++i) {

				if (node.getChild(i) != null)
					node.getChild(i).analyze(this);
			}
		}
	}
}
