function calcbalance()
%array of interest rates
interest=0.01 + (0.15-0.01).*rand(1,100);
%array of balance
bal=round(1000+(10000-1000).*rand(1,100));
%array of years
years=round(1+(10-1).*rand(1,100));
total_interest=0;
%calculate interest
for i=1:100
    total_interest=total_interest+(bal(i).*interest(i).*years(i));
end
%display total interest
disp(total_interests);
total_balance=0;
%calculate total available balance
for i=1:100
    total_balance=total_balance+bal(i);
end
%calculate total balance after adding interest
t=total_balance+total_interest;
%display total available balance
disp(t);
end
