package project.opt;

/**
 * @File Name : DeadCodeAnalysis.java
 * @Description : Class for doing dead code elimination 
 * 	It is BackwardAnalysis, if uses is not in currentOutSet,remove its definition
 * @author Mohit
 */
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import project.opt.ConstantPropogationAnalysis.Intst;


import natlab.toolkits.analysis.HashMapFlowMap;
import natlab.toolkits.analysis.HashSetFlowSet;
import natlab.toolkits.analysis.varorfun.VFPreorderAnalysis;
import natlab.utils.NodeFinder;
import nodecases.AbstractNodeCaseHandler;
import analysis.AbstractSimpleStructuralBackwardAnalysis;
import ast.ASTNode;
import ast.AssignStmt;
import ast.BinaryExpr;
import ast.EmptyStmt;
import ast.Expr;
import ast.LiteralExpr;
import ast.Name;
import ast.NameExpr;
import ast.ParameterizedExpr;
import ast.Script;
import ast.Stmt;

public class DeadCodeAnalysis extends
		AbstractSimpleStructuralBackwardAnalysis<HashSetFlowSet<String>> {
	private static VFPreorderAnalysis kindAnalysis;

	/**
	 * Static function for analysing program for dead code
	 * @param tree node of program
	 * @return analysis
	 */
	public static DeadCodeAnalysis of(ASTNode<?> tree) {
		//create object of VFPreorederAnlysis for doing kind analysis
		kindAnalysis = new VFPreorderAnalysis(tree);
		kindAnalysis.analyze();
		//create analyzer
		DeadCodeAnalysis analysis = new DeadCodeAnalysis(tree);
		analysis.analyze();
		return analysis;
	}
	/**
	 * Function for calling next stage
	 * @param flag
	 */
	public void nextStage(int flag) {
		//go to Forloop extractor class for doing pattern matching
		if(flag==0)
		ForloopExtraction.of(getTree());
	}

	/**
	 * Constructor for DeadCodeAnalysis
	 * @param tree -node of program
	 */
	private DeadCodeAnalysis(ASTNode tree) {
		super(tree);
	}

	/**
	 * Override Function for creating HashsetFlowset 
	 */
	@Override
	public HashSetFlowSet<String> newInitialFlow() {
		return new HashSetFlowSet<String>();
	}

	/**
	 * Override function for copy
	 */
	@Override
	public void copy(HashSetFlowSet<String> src, HashSetFlowSet<String> dest) {
		src.copy(dest);
	}

	
	/**
	 * Override function for merge
	 */
	@Override
	public void merge(HashSetFlowSet<String> in1, HashSetFlowSet<String> in2,
			HashSetFlowSet<String> out) {
		in1.union(in2, out);
	}

	/**
	 * Override function for caseStmt
	 */
	@Override
	public void caseStmt(Stmt node) {	
		outFlowSets.put(node, currentOutSet.copy());
		caseASTNode(node);
		currentInSet.copy(currentOutSet);
		inFlowSets.put(node, currentInSet.copy());
	}

	/**
	 * Override function for caseNameExpr
	 */
	@Override
	public void caseNameExpr(NameExpr nameExp) {
		//if nameexpr found,add to currentInSet checking that nameexpr is variable
		if (kindAnalysis.getResult(nameExp.getName()).isFunction() == false) {
			currentInSet.add(nameExp.getVarName());
		}
	}

	/**
	 * override function for caseAssignStmt
	 */
	@Override
	public void caseAssignStmt(AssignStmt node) {
		//put currentOutSet into outFlowSets
		outFlowSets.put(node, currentOutSet.copy());
		//get kill set
		Set<String> kill=new HashSet<>();
		if (!(node.getChild(0) instanceof ParameterizedExpr))
		 kill= node.getLValues();
		
		// compute (out - kill) + gen
		currentInSet = newInitialFlow();
		Iterator<String> itr3 = kill.iterator();
		while (itr3.hasNext()) {
		
			String temp = itr3.next();
			//if not in currentOutset means dead, remove that statement
			if (!currentOutSet.contains(temp)
					&& !node.getChild(1).toString().contains("RangeExpr")) {
				
				node.getParent().removeChild(
						node.getParent().getIndexOfChild(node));
			}
		}
		
		Set<String> outSet = currentOutSet.getSet();
		outSet.removeAll(kill);

		currentOutSet.clear();
		currentOutSet.addAll(outSet);
		currentOutSet.copy(currentInSet);
		//get gen set
		HashSetFlowSet<String> gen = new HashSetFlowSet<>();
		//get all nameexpression from the right side
		Iterator<NameExpr> itr = node.getRHS().getAllNameExpressions().iterator();
		while (itr.hasNext()) {
			NameExpr n = itr.next();
			//check that nameexpr is variable
			if (kindAnalysis.getResult(n.getName()).isFunction() == false)
				gen.add(n.getVarName());
		}
		//add gen set to currentInSet
		currentInSet.union(gen);
		inFlowSets.put(node, currentInSet.copy());
	}

	/**
	 * Override function for caseFunction
	 */
	@Override
	public void caseFunction(ast.Function node) {
		currentOutSet = newInitialFlow();
		currentInSet = currentOutSet.copy();
		outFlowSets.put(node, currentOutSet);
		node.getStmts().analyze(this);
		inFlowSets.put(node, currentInSet);
	}
	
	/**
	 * Override function for caseScript
	 */
	@Override
	public void caseScript(Script node) {
		currentOutSet = newInitialFlow();
		currentInSet = currentOutSet.copy();
		outFlowSets.put(node, currentOutSet);

		node.getStmts().analyze(this);

		inFlowSets.put(node, currentInSet);
	}	
}