package project.opt;
/**
 * @File Name : PatternChecking.java
 * @Description : Class for matching patterns and replace loops with library routines
 * @Author Mohit
 */
import java.util.List;

import project.pattern.CreatePattern;
import project.pattern.Pattern;
import project.pattern.ReplacePattern;
import project.pattern.TreeNode;
import ast.ASTNode;
import ast.ForStmt;

public class PatternChecking {

	public PatternChecking() {

	}
	//variable for pattern index
	private int patternindex = -1;
	
	/**
	 * Function for checking and replacing loop with library routine
	 * @param node ForStmt node
	 * @return ASTNode
	 */
	public ASTNode<?> checkAndReplace(ForStmt node) {
		ASTNode astnode = node;
		Pattern pattern = new Pattern();

		CreatePattern cpatterns = new CreatePattern();
		pattern = cpatterns.getPattern();

		List<TreeNode> patterns = pattern.getPatterns();
		//checkpattern1(node);
		for (int i = 0; i < patterns.size(); i++) {
			if(i==1){
				if (checkpattern(node, patterns.get(i), 6)) {
					// System.out.println("match found dot");
					patternindex = i;
					break;
				}
			}
			else if (i == 4) {
				if (checkpattern(node, patterns.get(i), 9)) {
					// System.out.println("match found nested loop zeros");
					patternindex = i;
					break;
				}
			} else {
				if (checkpattern(node, patterns.get(i), 4)) {
					 System.out.println("match found sum,zeros,ones");
					patternindex = i;
					break;
				}
			}
		}
		//if pattern found,replace with library routine
		//System.out.println(patternindex);
		if (patternindex != -1) {
			astnode = replaceNode(node, patternindex);
		}

		return astnode;
	}

	/**
	 * Function for replacing loop with library routine according to pattern index
	 * @param node ForStmt node
	 * @param patternindex2 pattern number in database
	 * @return ASTNode
	 */
	public ASTNode replaceNode(ForStmt node, int patternindex2) {
		ASTNode astnode = node;
		ReplacePattern rp=new ReplacePattern();
		switch (patternindex2) {
		case 0:
			// zeros or ones
			astnode = rp.getZerosorOnes(node);
			break;
		case 1:
			//dot
			astnode=rp.getDotNode(node);
			break;
		case 2:
			
			//sum
			astnode = rp.getSumNode(node);
			break;
		case 3:
			//prod
			astnode = rp.getProdNode(node);
			break;
		case 4:
			//nested zeros or ones
			astnode = rp.getNestedZerosorOnes(node);
			break;
		default:
			break;
		}
		return astnode;

	}

	/**
	 * Function for getting checking pattern
	 * @param node forStmt node
	 * @param treeNode pattern root node 
	 * @param c depth for checking
	 * @return true or false
	 */
	private boolean checkpattern(ASTNode node, TreeNode treeNode, int c) {
		String t1 = node.toString();
		String t2 = t1.substring(t1.indexOf('.') + 1, t1.indexOf('@'));
		//System.out.println(t2);
		// System.out.println(node.getNodeString());
		//System.out.println(treeNode.getData());
		// System.out.println(c);
		if (t2.equals(treeNode.getData())) {
			c = c - 1;
			if (c == 0) {
				return true;
			}
			List<TreeNode> list = treeNode.getChildren();
			boolean flag = true;
			//System.out.println("----" + node.getNumChild());
			//System.out.println("====" + list.size());
			
			//check pattern recursively
			if (list.size() == node.getNumChild()) {
				for (int i = 0; i < list.size(); i++) {
					// System.out.println(node.getChild(i));
					// System.out.println(list.get(i).getData());
					flag = flag
							&& checkpattern(node.getChild(i), list.get(i), c);
				}
				return flag;
			} else {
				return false;
			}
		}
		return false;
	}
}
