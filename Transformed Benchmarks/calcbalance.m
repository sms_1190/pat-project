function  [] = calcbalance()
  %array of interest rates
  interest = (0.01 + (0.14 .* rand(1, 10000)));
  %array of balance
  bal = round((1000 + ((10000 - 1000) .* rand(1, 10000))));
  %array of years
  years = round((1 + ((10 - 1) .* rand(1, 10000))));
  %calculate interest
  total_interest = sum(((bal((1 : 10000)) .* interest((1 : 10000))) .* years((1 : 10000))));
  %display total interest
  disp(total_interest);
  %calculate total available balance
  total_balance = sum(bal((1 : 10000)));
  %calculate total balance after adding interest
  t = (total_balance + total_interest);
  %display total available balance
  disp(t);
end
