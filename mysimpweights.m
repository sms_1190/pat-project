function mysimpweights()
% computes the weights for Simpsonís rule
% Input: n -- the number of intervals, must be even
% Output: a vector with the weights, length n+1
 
if rem(1000,2) ~= 0
error('n must be even')
end
 
for i=1:1000
    w(i)=1;
end
 
for i = 2:1000
if rem(i,2)==0
w(i)=4;
else
w(i)=2;
end
disp(w);
end
