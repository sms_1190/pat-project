function  [] = sample()
  
  %initialize a array
  a = zeros(1, 10000);
  disp(a);
  
  %initialize c array
  c = ones(1, 10000);
  
  %inialize e array
  e = ones(1, 10000);
  
  %pattern for dot
  dd = dot(c((1 : 10000)), e((1 : 10000)));
  disp(dd);
  
  %pattern for prod
  prod1 = prod(((c((1 : 1000)) .* e((1 : 1000))) * 2));
  disp(prod1);
  
end
