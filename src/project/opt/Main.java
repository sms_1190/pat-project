package project.opt;

/**
 * @File Name : Main.java
 * @Description : Main class which executes first
 * @author Mohit
 */
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


import mclint.util.Parsing;
import ast.ASTNode;
import ast.CompilationUnits;
import ast.Function;
import ast.FunctionList;
import ast.Program;
import ast.Script;

import com.google.common.base.Charsets;
import com.google.common.io.Files;


public class Main {
	
	/**
	 * Function for deleting existing files
	 * @param file
	 */
  private static void deleteRecursively(File file) {
    if (!file.exists()) {
      return;
    }
    if (file.isDirectory()) {
      for (File child : file.listFiles()) {
        deleteRecursively(child);
      }
    }
    file.delete();
  }
  
  /**
   * Function for creating instrumented directory
   * @param name
   * @return directory object
   */
  private static File createFreshDirectory(String name) {
    File dir = new File(name);
    deleteRecursively(dir);
    dir.mkdir();
    return dir;
  }

  public static void main(String[] args) throws IOException {
    // Parse the input files into an AST.
    CompilationUnits program = Parsing.files(args);

    //first analysis is constant propagation 
    Map<ASTNode<?>, ConstantPropogationAnalysis> analyses = new HashMap<ASTNode<?>, ConstantPropogationAnalysis>();
    for (Program unit : program.getPrograms()) {
      if (unit instanceof Script) {
        analyses.put(unit, ConstantPropogationAnalysis.of(unit));
      } else if (unit instanceof FunctionList) {
        for (Function f : ((FunctionList) unit).getFunctions()) {
          analyses.put(f, ConstantPropogationAnalysis.of(f));
        }
      }
    }
    //after analyzing put transformed code into new file
    for (ASTNode<?> node : analyses.keySet()) {
        analyses.get(node).nextStage();
      }
    // Pretty-print the modified program.
    File outputDirectory = createFreshDirectory("instrumented");
    for (Program unit : program.getPrograms()) {
      File outputFile = new File(outputDirectory, unit.getName() + ".m");
      Files.write(unit.getPrettyPrinted(), outputFile, Charsets.UTF_8);
    }
    

  }
}
