function  [] = mysimpweights()
  % computes the weights for Simpson�s rule
  % Input: n -- the number of intervals, must be even
  % Output: a vector with the weights, length n+1
    
  %initialize weights  
  w = ones(1, 10000);
  
  for i = (2 : 10000)
    if (rem(i, 2) == 0)
      w(i) = 4;
    else 
      w(i) = 2;
    end
    
  end
  disp(w);
end
