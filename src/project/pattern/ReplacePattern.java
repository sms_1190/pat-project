package project.pattern;

/**
 * @File Name : ReplacePattern.java
 * @Description : Class for replacing for loops with library routines
 * @author Mohit
 */
import java.math.BigDecimal;

import natlab.toolkits.analysis.varorfun.VFPreorderAnalysis;
import project.opt.Asts;
import ast.ASTNode;
import ast.AssignStmt;
import ast.BinaryExpr;
import ast.ETimesExpr;
import ast.Expr;
import ast.ForStmt;
import ast.IntLiteralExpr;
import ast.LiteralExpr;
import ast.MDivExpr;
import ast.MTimesExpr;
import ast.MinusExpr;
import ast.NameExpr;
import ast.ParameterizedExpr;
import ast.PlusExpr;
import ast.RangeExpr;

public class ReplacePattern {

	/**
	 * Function for simple for loop for zeros and ones patterns
	 * 
	 * @param node forstmt node
	 * @return ASTNode 
	 */
	public ASTNode getZerosorOnes(ForStmt node) {
		ASTNode astnode = node;
		String rangeindex;
		NameExpr varname = null;
		String varindex;
		int val = -1;
		// get i=1:n
		AssignStmt assignStmt1 = (AssignStmt) node.getChild(0);
		// get main stmt
		AssignStmt assignStmt2 = (AssignStmt) node.getChild(1).getChild(0);
		//get range index
		rangeindex = assignStmt1.getChild(0).getVarName();
		//get variable name
		varname = (NameExpr) assignStmt2.getChild(0).getChild(0);
		//get variable index
		varindex = assignStmt2.getChild(0).getChild(1).getChild(0).getChild(0)
				.getVarName();
		BigDecimal lval = new BigDecimal(0);
		IntLiteralExpr l = (IntLiteralExpr) assignStmt2.getChild(1);
		//get intliteral
		lval = new BigDecimal(l.getValue().getValue());
		val = lval.intValue();
		//get range expression
		RangeExpr r = (RangeExpr) assignStmt1.getChild(1);
		//checking varindex and rangeindex 
		if (varindex.equals(rangeindex)) {
			//if val==0,replace with zeros
			if (val == 0) {
				astnode = Asts.createzerosorones(0, r, varname);
			//if val==1,replace with ones	
			} else if (val == 1) {
				astnode = Asts.createzerosorones(1, r, varname);
			}
		}
		return astnode;
	}

	/**
	 * Function for nested for loops for zeros and ones patterns
	 * 
	 * @param node forstmt node
	 * @return ASTNode
	 */
	public ASTNode getNestedZerosorOnes(ForStmt node) {
		ASTNode astnode = node;
		String rangeindex;
		String rangeindex2;
		NameExpr varname = null;
		String varindex;
		String varindex2;
		IntLiteralExpr range1;
		IntLiteralExpr range2;
		int val = -1;
		// get i=1:n
		AssignStmt assignStmt1 = (AssignStmt) node.getChild(0);
		// get main stmt
		ForStmt forstmt = (ForStmt) node.getChild(1).getChild(0);
		// get j=1:n
		AssignStmt assignStmt2 = (AssignStmt) forstmt.getChild(0);
		// get two different range indexs
		rangeindex = assignStmt1.getChild(0).getVarName();
		rangeindex2 = assignStmt2.getChild(0).getVarName();
		// get twoo different ranges
		range1 = (IntLiteralExpr) assignStmt1.getChild(1).getChild(2);
		range2 = (IntLiteralExpr) assignStmt2.getChild(1).getChild(2);

		// get second list
		ast.List list = (ast.List) forstmt.getChild(1);
		// get main stmt
		AssignStmt assignStmt3 = (AssignStmt) list.getChild(0);
		// get variable name
		varname = (NameExpr) assignStmt3.getChild(0).getChild(0);
		// get variable indexs
		varindex = assignStmt3.getChild(0).getChild(1).getChild(0).getChild(0)
				.getVarName();
		varindex2 = assignStmt3.getChild(0).getChild(1).getChild(1).getChild(0)
				.getVarName();

		BigDecimal lval = new BigDecimal(0);
		IntLiteralExpr l = (IntLiteralExpr) assignStmt3.getChild(1);
		//get intliteral
		lval = new BigDecimal(l.getValue().getValue());
		val = lval.intValue();
		//checking for indexes of assigned variables
		if (varindex.equals(rangeindex) && varindex2.equals(rangeindex2)
				|| varindex.equals(rangeindex2) && varindex2.equals(rangeindex)) {
			if (val == 0) {
				astnode = Asts.nestedzerosorones(0, varname, varindex,
						varindex2, rangeindex, rangeindex2, range1, range2);

			} else if (val == 1) {
				astnode = Asts.nestedzerosorones(1, varname, varindex,
						varindex2, rangeindex, rangeindex2, range1, range2);
			}
		}
		return astnode;

	}

	/**
	 * Function for dot library routine pattern replacement
	 * 
	 * @param node ForStmt node
	 * @return ASTNode
	 */
	public ASTNode getDotNode(ForStmt node) {

		ASTNode astnode = node;
		String rangeindex;
		NameExpr lvarname = null;

		VFPreorderAnalysis reorderAnalysis = new VFPreorderAnalysis(node);
		reorderAnalysis.analyze();
		// get i=1:n
		AssignStmt assignStmt1 = (AssignStmt) node.getChild(0);
		// get main stmt
		AssignStmt assignStmt2 = (AssignStmt) node.getChild(1).getChild(0);

		// get range index
		rangeindex = assignStmt1.getChild(0).getVarName();
		// get left hand side variable name
		lvarname = (NameExpr) assignStmt2.getChild(0);
		PlusExpr plusexpr = (PlusExpr) assignStmt2.getChild(1);
		// get right hand side variable name
		NameExpr rvarname = (NameExpr) plusexpr.getChild(0);
		// get range expr
		RangeExpr r = (RangeExpr) node.getChild(0).getChild(1);
		//checking for assigned variable and variable in plus expression are same
		if (lvarname.getVarName().equals(rvarname.getVarName())) {
			if (plusexpr.getChild(1) instanceof MTimesExpr) {
				ASTNode astnode1 = plusexpr.getChild(1);
				if (checkExpression(plusexpr.getChild(1), reorderAnalysis,
						rangeindex)) {
					MTimesExpr mtimesexpr = (MTimesExpr) astnode1;
					// System.out.println(mtimesexpr.getNodeString());
					// System.out.println(mtimesexpr.getChild(0));
					NameExpr firstnameexpr = (NameExpr) mtimesexpr.getChild(0)
							.getChild(0);

					NameExpr secondnameexpr = (NameExpr) mtimesexpr.getChild(1)
							.getChild(0);

					astnode = Asts.createDotStmt(lvarname, firstnameexpr,
							secondnameexpr, r);
				}
			} else {

			}
		}

		return astnode;
	}

	/**
	 * Function for sum library routine pattern replacement
	 * 
	 * @param node ForStmt node
	 * @return ASTNode
	 */
	public ASTNode getSumNode(ForStmt node) {

		ASTNode astnode = node;
		String rangeindex;
		NameExpr lvarname = null;

		//variable for kind analysis
		VFPreorderAnalysis reorderAnalysis = new VFPreorderAnalysis(node);
		reorderAnalysis.analyze();
		// get i=1:n
		AssignStmt assignStmt1 = (AssignStmt) node.getChild(0);
		// get main stmt
		AssignStmt assignStmt2 = (AssignStmt) node.getChild(1).getChild(0);

		// get range index
		rangeindex = assignStmt1.getChild(0).getVarName();
		// get left hand side variable name
		lvarname = (NameExpr) assignStmt2.getChild(0);
		PlusExpr plusexpr = (PlusExpr) assignStmt2.getChild(1);
		// get right hand side variable name
		NameExpr rvarname = (NameExpr) plusexpr.getChild(0);
		// get range expr
		RangeExpr r = (RangeExpr) node.getChild(0).getChild(1);

		if (lvarname.getVarName().equals(rvarname.getVarName())) {
			if (plusexpr.getChild(1) instanceof BinaryExpr) {
				
				ASTNode astnode1 = checkandReplaceBinaryExpression(
						(BinaryExpr) plusexpr.getChild(1), node, rangeindex, r,
						lvarname, reorderAnalysis);
				if (node.equals(astnode1)) {

				} else {
					
					astnode = Asts.createSumStmt(lvarname, (Expr) astnode1);
				}
			} else {
				// check for simple paramterexpr
				if (plusexpr.getChild(1) instanceof ParameterizedExpr) {
					// get parameterizedExpr
					ParameterizedExpr prexpr = (ParameterizedExpr) plusexpr
							.getChild(1);
					// get name expression from parameterizedexpr
					NameExpr n = (NameExpr) prexpr.getChild(0);
					// check that variable is not function
					if (reorderAnalysis.getResult(n.getName()).isFunction() == true) {
						return astnode;
					} else {
						// check for index
						if (prexpr.getChild(1).getChild(0) instanceof NameExpr) {
							// get index
							NameExpr n1 = (NameExpr) prexpr.getChild(1)
									.getChild(0);
							String tvar = n1.getVarName();
							// check both rangeindex and variableindex are same
							if (tvar.equals(rangeindex)) {
								// change stmt with sum function
								astnode = Asts.createSumStmt(lvarname,
										n.getVarName(), r);
							}
						}
					}
				}

			}
		}

		return astnode;
	}

	/**
	 * Function for prod library routine pattern replacement
	 * 
	 * @param node ForStmt node
	 * @return ASTNode
	 */
	public ASTNode getProdNode(ForStmt node) {

		ASTNode astnode = node;
		String rangeindex;
		NameExpr lvarname = null;

		VFPreorderAnalysis reorderAnalysis = new VFPreorderAnalysis(node);
		reorderAnalysis.analyze();
		// get i=1:n
		AssignStmt assignStmt1 = (AssignStmt) node.getChild(0);
		// get main stmt
		AssignStmt assignStmt2 = (AssignStmt) node.getChild(1).getChild(0);

		
		// get range index
		rangeindex = assignStmt1.getChild(0).getVarName();
		// get left hand side variable name
		lvarname = (NameExpr) assignStmt2.getChild(0);
		MTimesExpr mtimesexpr = (MTimesExpr) assignStmt2.getChild(1);
		// get right hand side variable name
		NameExpr rvarname = (NameExpr) mtimesexpr.getChild(0);
		// get range expr
		RangeExpr r = (RangeExpr) node.getChild(0).getChild(1);
		//checking lefthand side variable with right hand side variable
		if (lvarname.getVarName().equals(rvarname.getVarName())) {
			if (mtimesexpr.getChild(1) instanceof BinaryExpr) {
				ASTNode astnode1 = checkandReplaceBinaryExpression(
						(BinaryExpr) mtimesexpr.getChild(1), node, rangeindex,
						r, lvarname, reorderAnalysis);
				if (node.equals(astnode1)) {

				} else {
					astnode = Asts.createProdStmt(lvarname, (Expr) astnode1);
				}
			} else {
				// check for simple paramterexpr
				if (mtimesexpr.getChild(1) instanceof ParameterizedExpr) {
					// get parameterizedExpr
					ParameterizedExpr prexpr = (ParameterizedExpr) mtimesexpr
							.getChild(1);
					// get name expression from parameterizedexpr
					NameExpr n = (NameExpr) prexpr.getChild(0);
					// check that variable is not function
					if (reorderAnalysis.getResult(n.getName()).isFunction() == true) {
						return astnode;
					} else {
						// check for index
						if (prexpr.getChild(1).getChild(0) instanceof NameExpr) {
							// get index
							NameExpr n1 = (NameExpr) prexpr.getChild(1)
									.getChild(0);
							String tvar = n1.getVarName();
							// check both rangeindex and variableindex are same
							if (tvar.equals(rangeindex)) {
								// change stmt with sum function
								astnode = Asts.createProdStmt(lvarname,
										n.getVarName(), r);
							}
						}
					}

				}

			}
		}

		return astnode;
	}

	/**
	 * Function for checking binary expression of sum or prod library routine
	 * pattern
	 * 
	 * @param child BinaryExpression node
	 * @param node parent node which is for loop
	 * @param rangeindex rangeindex of for loop
	 * @param r range expression
	 * @param lvarname left side variable
	 * @param reorderAnalysis parameter for kind analysis
	 * @return ASTNode 
	 */
	private ASTNode checkandReplaceBinaryExpression(ASTNode child,
			ForStmt node, String rangeindex, RangeExpr r, NameExpr lvarname,
			VFPreorderAnalysis reorderAnalysis) {
		ASTNode astnode = node;
		if (checkExpression(child, reorderAnalysis, rangeindex)) {
			
			astnode = replaceExpression((Expr) child, r);
		}
		return astnode;
	}

	/**
	 * Function for replacing expression with removing indexes with range
	 * 
	 * @param child node of expression
	 * @param r rangeexpression
	 * @return Expression with removing indexes with range
	 */
	public Expr replaceExpression(Expr child, RangeExpr r) {
		Expr astnode = child;
		//check that node is binary expression
		if (child instanceof BinaryExpr) {
			if (child instanceof PlusExpr) {
				astnode = new PlusExpr(replaceExpression(
						(Expr) child.getChild(0), r), replaceExpression(
						(Expr) child.getChild(1), r));
			} else if (child instanceof MTimesExpr) {
				astnode = new MTimesExpr(replaceExpression(
						(Expr) child.getChild(0), r), replaceExpression(
						(Expr) child.getChild(1), r));
			} else if (child instanceof MinusExpr) {
				astnode = new MinusExpr(replaceExpression(
						(Expr) child.getChild(0), r), replaceExpression(
						(Expr) child.getChild(1), r));
			} else if (child instanceof MDivExpr) {
				astnode = new MDivExpr(replaceExpression(
						(Expr) child.getChild(0), r), replaceExpression(
						(Expr) child.getChild(1), r));
			} else if (child instanceof ETimesExpr) {
				astnode = new ETimesExpr(replaceExpression(
						(Expr) child.getChild(0), r), replaceExpression(
						(Expr) child.getChild(1), r));
			}
		} else if (child instanceof ParameterizedExpr) {
			NameExpr n = (NameExpr) child.getChild(0);
			astnode = Asts.functionCall(n.getVarName(), r);

		}

		return astnode;
	}

	/**
	 * function for checking indexes of binary expression
	 * 
	 * @param child node of binary expression
	 * @param reorderAnalysis parameter of kind analysis
	 * @param rangeindex parameter for rangeindex
	 * @return true or false
	 */
	public boolean checkExpression(ASTNode child,
			VFPreorderAnalysis reorderAnalysis, String rangeindex) {
		
		boolean flag = true;
		//checking expression recursively
		if (child instanceof BinaryExpr) {
			
			flag = flag
					&& checkExpression(child.getChild(0), reorderAnalysis,
							rangeindex);
			flag = flag
					&& checkExpression(child.getChild(1), reorderAnalysis,
							rangeindex);
		} else if (child instanceof ParameterizedExpr) {
			ParameterizedExpr prexpr = (ParameterizedExpr) child;
			NameExpr n = (NameExpr) prexpr.getChild(0);
			NameExpr index = (NameExpr) prexpr.getChild(1).getChild(0);
			//checking for name expression not functions
			//System.out.println(n.getVarName());
			//System.out.println(reorderAnalysis.getResult(n.getName()).isFunction());
			if (reorderAnalysis.getResult(n.getName()).isFunction() == true) {
				flag = flag && false;
			}
			//checking for index of parameterized expression and rangeindex
			if (index.getVarName().equals(rangeindex)) {
				flag = flag && true;
			} else {
				flag = flag && false;
			}
		} else if (child instanceof LiteralExpr) {
			flag = flag && true;
		}
		
		return flag;
	}

}
