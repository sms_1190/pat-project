package project.pattern;

/**
 * @File Name : CreatePattern.java
 * @Description : This is class for creating pattern for library routine.
 * User can add his own pattern here
 * @Author : Mohit Shah
 */
import java.util.LinkedList;
import java.util.List;

public class CreatePattern {

	// variable object for pattern class
	private Pattern pattern;

	// constructor of the class.
	public CreatePattern() {
		this.pattern = new Pattern();
		crtpattern();

	}

	/**
	 * Function for creating patterns using treenode
	 */
	private void crtpattern() {
		// pattern for zeros or ones
		
		/*	Pattern :
		 * 	ForStmt -AssignStmt
		 * 				-NameExpr
		 * 					-Name
		 * 				-RangeExpr
		 * 					-IntLiteralExpr
		 * 					-Opt
		 * 					-IntLiteralExpr
		 * 			-List
		 * 				-AssignStmt
		 * 					-ParameterizedExpr
		 * 						-NameExpr
		 * 							-Name
		 * 						-List
		 * 							-NameExpr
		 * 								-Name
		 * 					-IntLiteralExpr
		 * 
		 */
		TreeNode root = new TreeNode("ForStmt");
		List<TreeNode> children = new LinkedList<>();
		children.add(new TreeNode("AssignStmt"));
		children.add(new TreeNode("List"));
		List<TreeNode> children0 = new LinkedList<>();
		children0.add(new TreeNode("NameExpr"));
		children0.add(new TreeNode("RangeExpr"));
		List<TreeNode> children1 = new LinkedList<>();
		children1.add(new TreeNode("ParameterizedExpr"));
		children1.add(new TreeNode("IntLiteralExpr"));

		List<TreeNode> children6 = new LinkedList<>();
		children6.add(new TreeNode("NameExpr"));
		children6.add(new TreeNode("List"));
		children1.get(0).setChildren(children6);

		List<TreeNode> children3 = new LinkedList<>();
		children3.add(new TreeNode("AssignStmt"));
		children3.get(0).setChildren(children1);

		List<TreeNode> children4 = new LinkedList<>();
		children4.add(new TreeNode("IntLiteralExpr"));
		children4.add(new TreeNode("Opt"));
		children4.add(new TreeNode("IntLiteralExpr"));
		children0.get(1).setChildren(children4);

		List<TreeNode> children5 = new LinkedList<>();
		children5.add(new TreeNode("Name"));
		children0.get(0).setChildren(children5);

		children.get(1).setChildren(children3);
		children.get(0).setChildren(children0);
		root.setChildren(children);
		this.pattern.addPattern(root);
		//pattern for dot 
		/*	Pattern :
		 * 	ForStmt -AssignStmt
		 * 				-NameExpr
		 * 					-Name
		 * 				-RangeExpr
		 * 					-IntLiteralExpr
		 * 					-Opt
		 * 					-IntLiteralExpr
		 * 			-List
		 * 				-AssignStmt
		 * 					-NameExpr
		 * 						-Name
		 * 					-PlusExpr
		 * 						-NameExpr
		 * 							-Name
		 * 						-MTimesExpr
		 * 							-ParameterizedExpr	
									-ParameterizedExpr
		 * */
		root = new TreeNode("ForStmt");

		children = new LinkedList<>();
		children.add(new TreeNode("AssignStmt"));
		children.add(new TreeNode("List"));

		children0 = new LinkedList<>();
		children0.add(new TreeNode("NameExpr"));
		children0.add(new TreeNode("RangeExpr"));

		children1 = new LinkedList<>();
		children1.add(new TreeNode("NameExpr"));
		children1.add(new TreeNode("PlusExpr"));

		children6 = new LinkedList<>();
		children6.add(new TreeNode("Name"));

		// sum --> NameExpr->Name leftsiide
		children1.get(0).setChildren(children6);
		List<TreeNode> children2 = new LinkedList<>();

		children2.add(new TreeNode("NameExpr"));
		children2.add(new TreeNode("MTimesExpr"));

		List<TreeNode> children7 = new LinkedList<>();
		children7.add(new TreeNode("Name"));
		// sum --> NameExpr->Name right side
		children2.get(0).setChildren(children7);
		
		List<TreeNode> children8=new LinkedList<>();
		children8.add(new TreeNode("ParameterizedExpr"));
		children8.add(new TreeNode("ParameterizedExpr"));
		children2.get(1).setChildren(children8);
		children3 = new LinkedList<>();
		children3.add(new TreeNode("AssignStmt"));
		// asignStmt --> nameexpr and plusexpr
		children3.get(0).setChildren(children1);

		children4 = new LinkedList<>();
		children4.add(new TreeNode("IntLiteralExpr"));
		children4.add(new TreeNode("Opt"));
		children4.add(new TreeNode("IntLiteralExpr"));
		children0.get(1).setChildren(children4);

		children5 = new LinkedList<>();
		children5.add(new TreeNode("Name"));
		children0.get(0).setChildren(children5);

		children1.get(1).setChildren(children2);
		children.get(1).setChildren(children3);
		children.get(0).setChildren(children0);
		root.setChildren(children);
		this.pattern.addPattern(root);
		
		
		/*	Pattern :
		 * 	ForStmt -AssignStmt
		 * 				-NameExpr
		 * 					-Name
		 * 				-RangeExpr
		 * 					-IntLiteralExpr
		 * 					-Opt
		 * 					-IntLiteralExpr
		 * 			-List
		 * 				-AssignStmt
		 * 					-NameExpr
		 * 						-Name
		 * 					-PlusExpr
		 * 						-NameExpr
		 * 							-Name
		 * 						-Expr
		 * 
		 * */
		root = new TreeNode("ForStmt");

		children = new LinkedList<>();
		children.add(new TreeNode("AssignStmt"));
		children.add(new TreeNode("List"));

		children0 = new LinkedList<>();
		children0.add(new TreeNode("NameExpr"));
		children0.add(new TreeNode("RangeExpr"));

		children1 = new LinkedList<>();
		children1.add(new TreeNode("NameExpr"));
		children1.add(new TreeNode("PlusExpr"));

		children6 = new LinkedList<>();
		children6.add(new TreeNode("Name"));

		// sum --> NameExpr->Name leftsiide
		children1.get(0).setChildren(children6);
		children2 = new LinkedList<>();

		children2.add(new TreeNode("NameExpr"));
		children2.add(new TreeNode("Expr"));

		children7 = new LinkedList<>();
		children7.add(new TreeNode("Name"));
		// sum --> NameExpr->Name right side
		children2.get(0).setChildren(children7);

		children3 = new LinkedList<>();
		children3.add(new TreeNode("AssignStmt"));
		// asignStmt --> nameexpr and plusexpr
		children3.get(0).setChildren(children1);

		children4 = new LinkedList<>();
		children4.add(new TreeNode("IntLiteralExpr"));
		children4.add(new TreeNode("Opt"));
		children4.add(new TreeNode("IntLiteralExpr"));
		children0.get(1).setChildren(children4);

		children5 = new LinkedList<>();
		children5.add(new TreeNode("Name"));
		children0.get(0).setChildren(children5);

		children1.get(1).setChildren(children2);
		children.get(1).setChildren(children3);
		children.get(0).setChildren(children0);
		root.setChildren(children);
		this.pattern.addPattern(root);

		// pattern for prod
		/*	Pattern :
		 * 	ForStmt -AssignStmt
		 * 				-NameExpr
		 * 					-Name
		 * 				-RangeExpr
		 * 					-IntLiteralExpr
		 * 					-Opt
		 * 					-IntLiteralExpr
		 * 			-List
		 * 				-AssignStmt
		 * 					-NameExpr
		 * 						-Name
		 * 					-MTimesExpr
		 * 						-NameExpr
		 * 							-Name
		 * 						-Expr
		 * 
		 * */
		root = new TreeNode("ForStmt");
		children = new LinkedList<>();
		children.add(new TreeNode("AssignStmt"));
		children.add(new TreeNode("List"));
		children0 = new LinkedList<>();
		children0.add(new TreeNode("NameExpr"));
		children0.add(new TreeNode("RangeExpr"));
		children1 = new LinkedList<>();
		children1.add(new TreeNode("NameExpr"));
		children1.add(new TreeNode("MTimesExpr"));

		children6 = new LinkedList<>();
		children6.add(new TreeNode("Name"));
		children1.get(0).setChildren(children6);
		children2 = new LinkedList<>();
		children2.add(new TreeNode("NameExpr"));
		children2.add(new TreeNode("Expr"));

		children7 = new LinkedList<>();
		children7.add(new TreeNode("Name"));
		children2.get(0).setChildren(children7);

		children3 = new LinkedList<>();
		children3.add(new TreeNode("AssignStmt"));
		children3.get(0).setChildren(children1);

		children4 = new LinkedList<>();
		children4.add(new TreeNode("IntLiteralExpr"));
		children4.add(new TreeNode("Opt"));
		children4.add(new TreeNode("IntLiteralExpr"));
		children0.get(1).setChildren(children4);

		children5 = new LinkedList<>();
		children5.add(new TreeNode("Name"));
		children0.get(0).setChildren(children5);

		children1.get(1).setChildren(children2);
		children.get(1).setChildren(children3);
		children.get(0).setChildren(children0);
		root.setChildren(children);
		this.pattern.addPattern(root);
		/**
		 * ForStmt
			-AssignStmt
				-NameExpr
					-Name
				-RangeExpr
					-IntLiteralExpr
					-Opt
					-IntLiteralExpr
			-List
				-ForStmt
					-AssignStmt
						-NameExpr
							-Name
						-RangeExpr
							-IntLiteralExpr
							-Opt
							-IntLiteralExpr
					-List
						-AssignStmt
							-ParameterizedExpr
								-NameExpr
									-Name
								-List
									-NameExpr
										-Name
									-NameExpr
										-Name
							-IntLiteralExpr
		 */
		root=new TreeNode("ForStmt");
		children1 = new LinkedList<>();
		children1.add(new TreeNode("AssignStmt"));
		
		children2=new LinkedList<>();
		children2.add(new TreeNode("NameExpr"));
		children3=new LinkedList<>();
		children3.add(new TreeNode("Name"));
		children2.get(0).setChildren(children3);
		
		children2.add(new TreeNode("RangeExpr"));
		children4=new LinkedList<>();
		children4.add(new TreeNode("IntLiteralExpr"));
		children4.add(new TreeNode("Opt"));
		children4.add(new TreeNode("IntLiteralExpr"));
		children2.get(1).setChildren(children4);
		
		children1.get(0).setChildren(children2);
		
		children1.add(new TreeNode("List"));
		children5=new LinkedList<>();
		children5.add(new TreeNode("ForStmt"));
		children6 = new LinkedList<>();
		children6.add(new TreeNode("AssignStmt"));
		
		children7=new LinkedList<>();
		children7.add(new TreeNode("NameExpr"));
		children8=new LinkedList<>();
		children8.add(new TreeNode("Name"));
		children7.get(0).setChildren(children8);
		
		children7.add(new TreeNode("RangeExpr"));
		children8=new LinkedList<>();
		children8.add(new TreeNode("IntLiteralExpr"));
		children8.add(new TreeNode("Opt"));
		children8.add(new TreeNode("IntLiteralExpr"));
		children7.get(1).setChildren(children8);
		children6.get(0).setChildren(children7);
		children6.add(new TreeNode("List"));
		//set children Assignment stmt into second for loop
		children5.get(0).setChildren(children6);
		children1.get(1).setChildren(children5);
		List<TreeNode> children9=new LinkedList<>();
		children9.add(new TreeNode("AssignStmt"));
		
		List<TreeNode> children10=new LinkedList<>();
		children10.add(new TreeNode("ParameterizedExpr"));
		
		List<TreeNode> children11=new LinkedList<>();
		children11.add(new TreeNode("NameExpr"));
		children7=new LinkedList<>();
		children7.add(new TreeNode("Name"));
		children11.get(0).setChildren(children7);
		
		children11.add(new TreeNode("List"));
		children5=new LinkedList<>();
		children5.add(new TreeNode("NameExpr"));
		children7=new LinkedList<>();
		children7.add(new TreeNode("Name"));
		children5.get(0).setChildren(children7);
		children5.add(new TreeNode("NameExpr"));
		children7=new LinkedList<>();
		children7.add(new TreeNode("Name"));
		children5.get(1).setChildren(children7);
		children11.get(1).setChildren(children5);
		
		children10.get(0).setChildren(children11);
		children10.add(new TreeNode("IntLiteralExpr"));
		children9.get(0).setChildren(children10);
		//set children as assignment Stmt into second list
		children6.get(1).setChildren(children9);
		
		//set children For in first list
		
		root.setChildren(children1);	
		pattern.addPattern(root);
	}

	/**
	 * Function for getting Pattern
	 * @return Pattern
	 */
	public Pattern getPattern() {
		return pattern;
	}

	/**
	 * Function for setting Pattern
	 * @param pattern
	 */
	public void setPattern(Pattern pattern) {
		this.pattern = pattern;
	}

}
