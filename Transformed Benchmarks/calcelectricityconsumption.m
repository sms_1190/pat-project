function  [] = calcelectricityconsumption()
  %power of devices
  w = (round((1 + ((100 - 1) * rand(1, 10000)))) * 10);
  %time in hour
  time = round((1 + ((24 - 1) * rand(1, 10000))));
  % charge in $ per kWH
  
  %initialize total_electricity charge
  total_electricity_charge=0;
  %calculate total electricity charges
  total_electricity_charge = sum((((w((1 : 10000)) .* time((1 : 10000))) ./ 1000) .* 0.109));
  %display total electricity consumption
  disp(total_electricity_charge);
end
