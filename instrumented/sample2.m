function  [] = sample2()
  
  
  %inialize c array
  c = ones(1, 10000);
  
  %inialize e array
  e = ones(1, 10000);
  
  %pattern for sum
  sum1 = sum(((c((1 : 1000)) .* e((1 : 1000))) * 40));
  disp(sum1);
  
  avg = (sum1 / 1000);
  disp(avg);
  
  %another for loop for sum but will not convert
  sum2 = 0;
  for t = (1 : 10)
    sum2 = (sum2 + sin(t));
  end
  disp(sum2);
  %pattern for nested for loop of zeros
  s = zeros(100, 50);
  disp(s);
  
  t1 = ones(50, 100);
  
  disp(t1);
end
