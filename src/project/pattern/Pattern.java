package project.pattern;
/**
 * @File Name : Pattern.java
 * @Description : Class for storing patterns
 * @author Mohit
 */
import java.util.LinkedList;
import java.util.List;

import ast.ASTNode;
import ast.ForStmt;

public class Pattern {

	//variable for storing pattern's root
	private List<TreeNode> patterns;
	/**
	 * Constructor for Pattern class
	 */
	public Pattern(){
		patterns=new LinkedList<>();
	}
	
	/**
	 * Function for adding patterns
	 * @param root
	 */
	public void addPattern(TreeNode root){
		patterns.add(root);
	}

	/**
	 * Function for getting Patterns
	 * @return List of TreeNodes
	 */
	public List<TreeNode> getPatterns() {
		return patterns;
	}

	/**
	 * Function for setting patterns
	 * @param patterns
	 */
	public void setPatterns(List<TreeNode> patterns) {
		this.patterns = patterns;
	}
	
	
}
