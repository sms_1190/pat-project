package project.opt;

/**
 * @File Name : CopyAssignmentChanger.java
 * @Description : Class for changing usage of variable with defs
 * @author Mohit
 */
import java.util.Iterator;
import java.util.Set;

import natlab.toolkits.analysis.HashMapFlowMap;
import ast.ASTNode;
import ast.AssignStmt;
import ast.BinaryExpr;
import ast.LiteralExpr;
import ast.ParameterizedExpr;

public class CopyAssignmentChanger {

	public CopyAssignmentChanger() {

	}

	/**
	 * Function for changing usage of variable with defs
	 * 
	 * @param child
	 * @param currentInSet
	 */
	public void changer(ASTNode child,
			HashMapFlowMap<String, Set<AssignStmt>> currentInSet) {
		//System.out.println(child.getNodeString());
		if (!(child instanceof LiteralExpr)) {
			
			for (int i = 0; i < child.getNumChild(); i++) {
				// change children recursively
				if (child.getChild(i) instanceof BinaryExpr) {
					this.changer(child.getChild(i), currentInSet);
				} else {
					String t1 = child.getChild(i).getVarName();
					if (currentInSet.containsKey(t1)
							&& !(child.getChild(i) instanceof ParameterizedExpr)) {
						Set<AssignStmt> set1 = currentInSet.get(t1);
						Iterator<AssignStmt> itr1 = set1.iterator();
						int count = 0;
						while (itr1.hasNext()) {
							count = count + 1;
							itr1.next();
						}
						if (count == 1) {
							child.removeChild(i);
							Iterator<AssignStmt> itr2 = set1.iterator();
							while (itr2.hasNext()) {
								child.insertChild(itr2.next().getChild(1), i);
							}
							
						}
					}
				}
			}
		}
	}

}
